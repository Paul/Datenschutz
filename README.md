# Datenschutzfragebogen

Dieser Katalog dient dem Erstellen eines **Datenschutzkonzepts** für
Forschungsprojekte und Anwendungen. Der Katalog wurde an der
Philipps-Universität Marburg entwickelt und ist auf Anwender_innen
ausgelegt, die ohne Vorwissen ein DSGVO-konformes Datenschutzkonzept
erstellen wollen. Der Katalog ist darauf ausgelegt, Hinweise und
Beispielantworten bereitzustellen um den Anforderungen an den
Datenschutz gerecht zu werden. Sowohl im Fragebogen als auch in der
*Ansicht* "Datenschutzkonzept" finden Anwender_innen hilfreiche Hinweise.
**Der Output dieses Fragebogens wird durch die Ansicht
*Datenschutzkonzept* dargestellt. Dieser Output ist als erster Entwurf
eines Datenschutzkonzepts zu verstehen. Dieses Produkt kann keine
rechtliche Beratung oder Prüfung ersetzen!**

Um den Katalog zu importieren, befolgen Sie folgende Schritte **in der
angegebenen Reihenfolge**. Wird die Reihenfolge nicht beachtet treten im
Katalog und in der Ansicht Fehler auf!

 1. Importieren Sie die Datei "[Attribute.xml](Attribute.xml)" im Managementbereich unter
*Domäne*
 2. Importieren Sie die Datei "[Optionen.xml](Optionen.xml)" im Managementbereich unter
*Optionen*
 3.  Importieren Sie die Datei "[Bedingungen.xml](Bedingungen.xml)" im Managementbereich
unter *Bedingungen*
 4.  Importieren Sie die Datei "[Fragen.xml](Fragen.xml)" im Managementbereich unter
*Fragen*
 5.  Importieren Sie die Datei "[Ansichten.xml](Ansichten.xml)" im Managementbereich
unter *Ansichten*

**Die Datei [Ansichten.xml](Ansichten.xml) funktioniert leider nicht einwandfrei: Die Ausgabe von Sets (Tabellenblock [Zeile 204-217](Ansichten.xml#L204-217) sowie [451-462](Ansichten.xml#L451-462)) funktioniert leider nicht wie in der [Dokumentation](https://rdmo.readthedocs.io/en/latest/management/views.html) beschrieben.**

Vor oder nach dem Import der Datei "[Ansichten.xml](Ansichten.xml)" müssen dort Angaben
über die eigene Einrichtung ergänzt werden. In der Datei wird an mehrern Stellen auf die Philipps-Universität Marburg verwiesen. Anstelle dieser Verweise sollten Sie Verweise auf Ihre eigene Einrichtung verwenden. Beispiele:
1. [Zeile 82 "Rechtsabteilung der Philipps-Universität Marburg"](Ansichten.xml#L82-83)
2. [Zeile 96-98 (Kontaktdaten der Philipps-Universität)](Ansichten.xml#L96-98)

Bitte prüfen Sie genau, welche Angaben geändert werden müssen.
